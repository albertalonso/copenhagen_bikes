import numpy as np
from matplotlib import pyplot as plt


fig, ax = plt.subplots(1,2, figsize = (10,5))
img = np.load('img_no_dare.npy')
ax[0].imshow(np.flipud(img[:,:]), aspect = 'auto', extent = [0,7/8,0,4], cmap = 'coolwarm')
ax[0].set_xlabel('Density')
ax[0].set_ylabel('Speed')

for i in range(3,img.shape[1]):
    #  plt.bar(np.linspace(0,4,100, endpoint = True), img[:,i])
    ax[1].plot(np.linspace(0,4,100,endpoint = True), img[:,i] + 1*i, color = 'black', alpha = 0.3)
ax[1].set_ylabel('Density [AU]')
ax[1].set_xlabel('Speed1')

fig.savefig('Gaussian_bikes_no_dare', dpi = 400)
