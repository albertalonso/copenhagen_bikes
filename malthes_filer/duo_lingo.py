import numpy as np
import tqdm


data = np.arange(1,11,1)

np.random.shuffle(data)

data = data.reshape(2,5,1)

ID = data.copy()

data = np.concatenate((data, np.random.uniform(0,1,10).reshape(2,5,1)), axis = 2)

def translator(ID, road, initial):
    bikes_to_cover = list(set(ID.ravel()))
    new_road = np.zeros_like(road)
    for i, bike in enumerate(bikes_to_cover):
        if bike <= 0:
            continue
        locx, locy = np.where(ID == bike)
        new_road[locx[0], locy[0], :] = road[int(initial[0,i]), int(initial[1,i]), :] 
    return new_road



def inital_setup(road):
    bikes_to_cover = list(set(road[:,:,-1].ravel()))
    loc = np.zeros((2,len(bikes_to_cover)))
    for i, bike in enumerate(bikes_to_cover):
        if bike <= 0:
            continue
        locx, locy = np.where(road[:,:,-1] == bike)
        loc[0,i] = locx
        loc[1,i] = locy

    return loc
        
def duo_lingo(data, ID):
    iterations = ID.shape[-1]
    inital_loc = inital_setup(data)
    final_road = np.zeros((data.shape[0], data.shape[1],data.shape[2], iterations))
    for i in tqdm.tqdm(range(iterations)):
        #  ID = np.roll(ID,np.random.randint(1,3,1), axis = 1)
        ID_current = ID[...,i]
        final_road[:,:,:,i] = translator(ID_current, data, inital_loc)
    return final_road

ID = '/run/media/nordentoft/backup/data/Niter=50000_roadlen=400_Nbike=400_Ncbike=5_maxspeed=5_ran=0.01.npy'
data = '/run/media/nordentoft/backup/data/Niter=50000_roadlen=400_Nbike=400_Ncbike=5_maxspeed=5_ran=0.1_initial.npy'

ID = np.load(ID)
data = np.load(data)

final = duo_lingo(data, ID)
np.save('/run/media/nordentoft/backup/carl', final)




