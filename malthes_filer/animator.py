import numpy as np
from matplotlib import pyplot as plt
from matplotlib import patches
from scipy import stats
import sys
import os
from celluloid import Camera
import tqdm

road_rotation = 0


fig, ax = plt.subplots(subplot_kw = {'projection':'polar'})
camera = Camera(fig)
ax.set_axis_off()

def colourizer(risk_factor, rgb_value = np.array([255, 3, 43])/301): #/301
    return [(rgb_value*risk_factor)]




print('one')
#  data  = np.load(f'.{os.sep}low_congestion_high_spread.npy')
data = np.load('/run/media/nordentoft/backup/carl.npy')
print('two')
phi = np.linspace(0,2*np.pi, data.shape[1], endpoint = True)
print(data.shape)


for t in tqdm.tqdm(range (int(data.shape[3]/500))):
    road = data[:,:,:,t]
    for j in range(road.shape[1]):       
        for i in range(2):
                if road[i,j,0] != 0:
                    if road[i,j,0] == 1:
                            ax.scatter(phi[j]+np.diff(phi)[0]*t*road_rotation, 1+(i*.1), color = 'black', marker = 'x', zorder = 30)
                            #  ax.scatter(phi[j], 1+(i*.1), color = 'black', marker = 'x', zorder = 30)
                            ax.grid = True
                    else:
                            ax.scatter(phi[j]+np.diff(phi)[0]*t*road_rotation, 1+(i*.1), c= colourizer(road[i,j,1]/6) , zorder = 30)
                            ax.grid = True
                            #  ax.scatter(phi[j], 1+(i*.1), zorder = 30)
    ax.fill(phi, np.ones(road.shape[1])*1.2, fill=True, color = 'gainsboro', zorder = 1)
    ax.fill(phi, np.ones(road.shape[1])*0.9, fill=True, color = 'white', zorder = 2)
    camera.snap()


animation = camera.animate()
animation.save('animation_quick.mp4', dpi= 300)
