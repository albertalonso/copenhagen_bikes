import numpy as np
from matplotlib import pyplot as plt
import glob 
import re


def density(IDs):
    IDs = np.load(IDs)
    L = IDs.shape[1]
    IDs[IDs > 0] = 1
    IDs[IDs < 0] = 0
    p1 = IDs[0,...].sum(axis = 0)/L
    p2 = IDs[1,...].sum(axis = 0)/L
    return p1.mean(), p2.mean()


def end_speed(IDs):
    IDs = np.load(IDs)
    L = IDs.shape[1]
    IDs = IDs*1.0001
    IDs = (IDs%1)*2
    v1 = IDs[0,...].sum(axis = 0)/L
    v2 = IDs[1,...].sum(axis = 0)/L
    return v1, v2



    #  plt.plot(p1/(p1 + p2))
    #  plt.plot(p2/(p1 + p2))
    #  plt.ylim(0,1)
    #  plt.show()


fnames = glob.glob('one_speed/*.npy')
p1 = []
p2 = []
v1 = []
v2 = []
rand = []
for fname in fnames:
    if re.search('_initial', fname) == None:
        if re.search('_hist', fname) != None:
            continue
        ran = float(fname.split('=')[-1][:-4])
        CB = int(fname.split('=')[-3].split('_')[0])
        print(CB)
        P1, P2 = density(fname)
        V1, V2 = end_speed(fname)
        p1.append(P1)
        p2.append(P2)
        v1.append(V1)
        v2.append(V2)
        rand.append(CB)


np.save('p1_0_inner_100_ran', p1)
np.save('p2_0_inner_100_ran', p2)
np.save('v1_0_inner_100_ran', v1)
np.save('v2_0_inner_100_ran', v2)
np.save('CB_0_inner_100_ran', rand)

plt.scatter(rand, p1)
plt.scatter(rand, p2)
plt.savefig('cb_vs')
plt.show()




#  ID = '/run/media/nordentoft/backup/data/Niter=50000_roadlen=400_Nbike=400_Ncbike=5_maxspeed=5_ran=0.1.npy'
#  density(ID)
#  ID = '/run/media/nordentoft/backup/data/Niter=50000_roadlen=400_Nbike=400_Ncbike=5_maxspeed=5_ran=0.2.npy'
#  density(ID)
#  ID = '/run/media/nordentoft/backup/data/Niter=50000_roadlen=400_Nbike=400_Ncbike=5_maxspeed=5_ran=0.3.npy'
#  density(ID)
    

