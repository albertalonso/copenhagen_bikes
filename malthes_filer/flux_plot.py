import numpy as np
from matplotlib import pyplot as plt
import glob
import re


fnames = glob.glob('/home/zwx588_ku_dk/modi_mount/cykel/carls_filer/one_speed/*_initial.npy')
density_bin = []
flux_mean = []
flux_std = []
for fname in fnames:
    N = fname.split('=')[3]
    N = int(N.split('_')[0])
    flux = np.load(fname)
    flux_mean.append(np.mean(flux))
    flux_std.append(np.std(flux))
    density_bin.append(N/(400*2))

fig, ax = plt.subplots(1,1, figsize = (10,10))
ax.errorbar(density_bin, flux_mean, yerr = flux_std, ls = 'None', color = 'crimson')
ax.set(xlabel = 'Density', ylabel = 'Flux')

fig.savefig('sejt_plot')



