import numpy as np
from matplotlib import pyplot as plt
from matplotlib import patches
import sys
import glob
import tqdm
import re
from multiprocessing import Pool, cpu_count

def func(road, val):
    time_since = 0
    avg_speed = []
    avg_time = []
    passed = False
    desired_speed = 0
    for i in range(road.shape[-1]):
        if (road[:, 100:103,i] == val).any():
            passed = True
        if (road[:,10:13, i] == val).any():
            if passed == True:
                avg = 400/(i - time_since)
                avg_speed.append(avg)
                avg_time.append(i)
                time_since = i
                passed = False

    return avg_speed, avg_time

def loader(ID, init, N_bikes):
    ID = np.load(ID) 
    init = np.load(init)
    speed_1 = []
    speed_2 = []
    speed_3 = []
    speed_4 = []
    for bike_ID in (range(1,int(N_bikes))):
        loc_x, loc_y = np.where(init[:,:,-1] == bike_ID)
        desired_speed = init[loc_x, loc_y, 1]
        avg_speed, avg_time = func(ID, bike_ID)

        if desired_speed == 1:
            speed_1.append(avg_speed)
        elif desired_speed == 2:
            speed_2.append(avg_speed)
        elif desired_speed == 3:
            speed_3.append(avg_speed)
        else:
            speed_4.append(avg_speed)

    val = np.zeros((100))
    if len(speed_1) > 0:
        speed_1 = np.hstack(speed_1)
        val1, _ = np.histogram(speed_1, bins = np.linspace(0,4,101, endpoint = True), density = True)
        val += val1
    if len(speed_2) > 0:
        speed_2 = np.hstack(speed_2)
        val2, _ = np.histogram(speed_2, bins = np.linspace(0,4,101, endpoint = True), density = True)
        val += val2
    if len(speed_3) > 0:
        speed_3 = np.hstack(speed_3)
        val3, _ = np.histogram(speed_3, bins = np.linspace(0,4,101, endpoint = True), density = True)
        val += val3
    if len(speed_4) > 0:
        speed_4 = np.hstack(speed_4)
        val4, _ = np.histogram(speed_4, bins = np.linspace(0,4,101, endpoint = True), density = True)
        val += val4
    #avg_speed = np.hstack(speed_bin)
    #val, _ = np.histogram(avg_speed, bins = np.linspace(0,4,100, endpoint = True), density = True)
    return val

#find all ID 

def runner(i):
    fnames = glob.glob('/run/media/nordentoft/backup/data/*.npy')
    IDs = []
    IDs_ran = []

    dat = []
    dat_ran = []
    for fname in fnames:
        if re.search('_initial', fname) == None:
            IDs.append(fname)
            ran = float(fname.split('=')[-1][:-4])
            IDs_ran.append(ran)

        else:
            dat.append(fname)
            ran = float(fname.split('=')[-1][:-12])
            dat_ran.append(ran)
    dat = np.array(dat)
    IDs = np.array(IDs)

    dat = dat[np.argsort(dat_ran)]
    IDs = IDs[np.argsort(IDs_ran)]


    val = loader(IDs[i], dat[i], 400)
    np.save(f'{ID[:-4]}_hist', val)




if __name__ == '__main__':
    fnames = glob.glob('/run/media/nordentoft/backup/data/*.npy')
    IDs = []
    IDs_ran = []

    dat = []
    dat_ran = []
    for fname in fnames:
        if re.search('_initial', fname) == None:
            IDs.append(fname)
            ran = float(fname.split('=')[-1][:-4])
            IDs_ran.append(ran)

        else:
            dat.append(fname)
            ran = float(fname.split('=')[-1][:-12])
            dat_ran.append(ran)
    dat = np.array(dat)
    IDs = np.array(IDs)

    dat = dat[np.argsort(dat_ran)]
    IDs = IDs[np.argsort(IDs_ran)]
    N = len(dat)

    pool = Pool(cpu_count()-2)
    res = pool.map(runner, np.arange(0,N,1))
    pool.close()
    pool.join()
#
