import argparse
from engine import Engine 
import numpy as np

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--n_iterations', '-N', type=int, default=1000, required=True,
                    help='an integer for the number of iterations')
parser.add_argument('--road_length', '-L', type=int, default=200, required=False,
                    help='an integer for the lenght of the road')
parser.add_argument('--n_bikes', type=int, default=10, required=False,
                    help='an integer for the number of bikes')
parser.add_argument('--n_cbikes', type=int, default=5, required=False,
                    help='an integer for the number of cargo bikes')
parser.add_argument('--max_speed', type=int, default=5, required=False,
                    help='an integer for maximum speed')
parser.add_argument('--savepath', type=str, default='./data/', required=False,
                    help='dir in which to save output datafile')
parser.add_argument('--filename', type=str, default='el classico', required=False,
                    help='name of output datafile')
parser.add_argument('--ran', '-r', type=float, default='0.1', required=True,
                    help='Randomness of the riders')
args = parser.parse_args()

if args.filename == 'el classico':
    args.filename = (f'Niter={args.n_iterations}_roadlen={args.road_length}'
                    f'_Nbike={args.n_bikes}_Ncbike={args.n_cbikes}_maxspeed={args.max_speed}_ran={args.ran}')
savename= args.savepath + args.filename
saveinit= args.savepath + args.filename + '_initial'
#print(savename)


def func(road, val):
    time_since = 0
    avg_speed = []
    avg_time = []
    passed = False
    desired_speed = 0
    for i in range(road.shape[-1]):
        if (road[:, 100:103,4,i] == val).any():
            passed = True
            if desired_speed == 0:
                for k in range(100,103):
                    for j in range(2):
                        if road[j,k,4,i] == val:
                            desired_speed = road[j,k,1,i]
        if (road[:,10:13, 4, i] == val).any():
            if passed == True:
                avg = 400/(i - time_since)
                avg_speed.append(avg)
                avg_time.append(i)
                time_since = i
                passed = False

    return avg_speed, avg_time, desired_speed

def flux_in_time(road):
    max_speed = 10
    start = 100
    back = np.zeros((2,max_speed)).ravel()
    back_twice = np.zeros_like(back)
    
    flux = []
    old_IDS = [0]
    for i in range(road.shape[-1]):
        front = road[:,start-max_speed:start,4,i].ravel()
        #  front_and_back = np.hstack([front.ravel(), back.ravel()])
        #  unq = []
        FLUX = 0
        for b in back:
            if b not in front and b != 0:
                FLUX += 1
        
        #  for item, count in Counter(front_and_back).most_common():
        #      if count == 1 and item != 0:
        #          unq.append(item)
        #  FLUX = len(unq)
        #  for i in range(unq):
        #      if not unq in(old_IDS):
        #          FLUX -= 1
        flux.append(FLUX)
        back = front.copy()
        #  back_twice = back.copy()
    return flux

def loader(road, N_bikes):
    speed_1 = []
    speed_2 = []
    speed_3 = []
    speed_4 = []
    for bike_ID in range(1,int(N_bikes)):
        avg_speed, avg_time, desired_speed = func(road, bike_ID)
        print(bike_ID, desired_speed)
        if desired_speed == 1:
            speed_1.append(avg_speed)
        elif desired_speed == 2:
            speed_2.append(avg_speed)
        elif desired_speed == 3:
            speed_3.append(avg_speed)
        else:
            speed_4.append(avg_speed)

    val = np.zeros((100))
    if len(speed_1) > 0:
        speed_1 = np.hstack(speed_1)
        val1, _ = np.histogram(speed_1, bins = np.linspace(0,4,101, endpoint = True), density = True)
        val += val1
    if len(speed_2) > 0:
        speed_2 = np.hstack(speed_2)
        val2, _ = np.histogram(speed_2, bins = np.linspace(0,4,101, endpoint = True), density = True)
        val += val2
    if len(speed_3) > 0:
        speed_3 = np.hstack(speed_3)
        val3, _ = np.histogram(speed_3, bins = np.linspace(0,4,101, endpoint = True), density = True)
        val += val3
    if len(speed_4) > 0:
        speed_4 = np.hstack(speed_4)
        val4, _ = np.histogram(speed_4, bins = np.linspace(0,4,101, endpoint = True), density = True)
        val += val4
    #avg_speed = np.hstack(speed_bin)
    #val, _ = np.histogram(avg_speed, bins = np.linspace(0,4,100, endpoint = True), density = True)
    return val, N_bikes

def save_constructer(iterations, road):
    return np.zeros((road.shape[0], road.shape[1], road.shape[2], iterations))


BikeMethod = Engine(args.n_iterations, args.n_bikes, args.n_cbikes, savename, args.road_length,
        args.max_speed, args.ran )
#print(BikeMethod.road[:,:,0])

def save_ID(sav):
    return flux_in_time(sav[:,:,-1,:])

def save_initial(sav):
    return sav[:,:,:,-1]



Sav = save_constructer(BikeMethod.N_itrs, BikeMethod.road)
for i in (range(BikeMethod.N_itrs)):
    #print(i)
    BikeMethod.iterate()
    Sav[...,i] = BikeMethod.road

#  val = loader(Sav, BikeMethod.N_bikes) # counter var her
np.save(BikeMethod.savepath, save_ID(Sav))
#np.save(saveinit, save_initial(Sav))




#np.save(f'./data/iter_{sys.argv[1]}_Length_{sys.argv[2]}_Nbike_{sys.argv[3]}_Ncb_{sys.argv[4]}_maxspeed_{sys.argv[5]}', Sav)
