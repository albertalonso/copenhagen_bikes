import numpy as np

'''
road attribute index 3:
0 = street level, which types
1 = desired speed
2 = actual speed
3 = dare
4 = ID

'''

class Engine:
    def __init__(self, N_iterations, number_of_bikes, number_of_cbikes, savepath, road_length, max_speed, randomness, initial_road=None, random_move = True):

        self.N_itrs = N_iterations
        self.N_bikes = number_of_bikes
        self.N_cbikes = number_of_cbikes
        self.savepath = savepath
        self.max_speed = max_speed
        self.random_move = random_move
        self.random_level = randomness
        self.bike_IDs = np.empty(self.N_bikes)

        if initial_road == None:
            self.road_length = road_length 
            self.setup_road()
        else:
            self.road = initial_road
            self.road_length = initial_road.shape[1]

    def setup_road(self):
        self.road = np.zeros((2,self.road_length,5))

        bike_pos_horiz = np.random.choice(np.arange(0,self.road_length*2,1), self.N_bikes, replace = False)
        bike_pos_verti  = np.zeros(self.N_bikes)

        for i in range(self.N_bikes):
            if bike_pos_horiz[i] >= self.road_length:
                lane = 1
                bike_pos_horiz[i] -= (self.road_length + 1)
            else:
                lane = 0
            bike_pos_verti[i] = lane

        for i in range(len(bike_pos_horiz)):
            dare = (np.random.vonmises(.5,.5,1)+np.pi)/(2*np.pi)
            if dare < 0:
                dare = 0
            if dare > 1:
                dare = 1
            self.road[int(bike_pos_verti[i]), int(bike_pos_horiz[i]), :] = [2, np.random.randint(1,int(self.max_speed),1)[0], 1, dare, 0]
            #for k in range(2):
                #road[k, i, :] = [2, np.random.randint(1,int(sys.argv[5]),1)[0], 1, np.random.uniform(0,1,1)[0], 0]

        #cbike_pos_horiz = np.random.randint(0,self.road_length, int(self.N_cbikes)) # the old way
        cbike_pos_horiz = np.random.choice(np.arange(0,self.road_length,1)[self.road[0,:,0] == 0], self.N_cbikes , replace = False)

        for cbike_pos in cbike_pos_horiz:
            self.road[0,cbike_pos,:] = [1, 0, 0 ,0, 0]


        #assigning ids
        bike_ID = 1 
        CB_ID   = -1
        for j in range(self.road.shape[1]):
            for i in range(self.road.shape[0]):
                if self.road[i,j,0] > 1:
                    self.road[i,j,4] = bike_ID
                    bike_ID += 1
                elif self.road[i,j,0] == 1:
                    self.road[i,j,4] = CB_ID
                    CB_ID -= 1


        #  self.bike_IDs = self.road[:,:,-1][self.road[:,:,-1] > 0]
        self.bike_IDs = np.arange(1,bike_ID, 1)
        print('setup end')
        print('COUNTER', bike_ID, self.N_bikes)
        print(self.road[:,:,0])
        print(self.road.shape)

    def iterate(self):
        length = self.road.shape[1]
        lane = np.concatenate((np.zeros(length), np.ones(length)))
        place = np.concatenate((np.arange(0,length,1), np.arange(0,length,1)))

        road_index = np.vstack([lane,place])

        index = np.arange(0,2*length,1)


        np.random.shuffle(self.bike_IDs)

        for i, ID in enumerate(self.bike_IDs):
            lane, place = np.where(self.road[:,:,4] == ID)
            lane = int(lane[0])
            place = int(place[0])

            if self.random_level > np.random.uniform(0,1,1):
                if self.road[lane,place,0] != 0:
                    self.road = self.random_move_updater(lane, place)

            else: # for normal driving
                if self.road[lane,place,0] != 0:
                    self.road = self.updater(lane, place)

    def updater(self, i, j):
        speed = self.road[i, j, 2]  
        if speed < self.road[i, j, 1]:
            speed += 1
        elif speed > self.road[i,j,1]:
            speed = self.road[i,j,1]
        new_lane, new_speed, new_pos = self.p_sight(speed, i, j)
        
        if new_lane == 1 and i == 0: #Check to see if we dare overtake
            if np.random.uniform(0,1,1)[0] > self.road[i,j,3]:
                #  print('I did not dare', self.road[i,j,3])
                #  if true we did not dare to pull out and we stay behind for atleast one iteration
                x, new_speed, new_pos = self.p_front(speed, i, j)
        
        self.road[i,j,2] = new_speed

        print('Speed', new_speed, 'jump_lengt', new_pos -1 - j, 'Desire', self.road[i,j,1])

        if self.road[int(new_lane), int(new_pos-1),0] != 0:
            if j == new_pos -1:
                1 + 1
                
            else: 
                print(i,j, self.road[i,j,2])
                print(new_lane, new_speed, new_pos - 1)
                print('problem')
                self.road[i,j,0] += 100
                self.road[int(new_lane),int(new_pos-1),0] += 200
                print(new_pos - 1)
                print(self.road[:,:,0])
                exit()

        if new_speed == 0:
            return self.road

        else:
            self.road[int(new_lane), int(new_pos - 1), :] = self.road[i, j, :]
            self.road[i,j,:] = 0
            return self.road

    def random_move_updater(self, i, j):
        'Choose whether to decrease speed or brake, randomly. Update self.road accordingly'
        random_type = np.random.choice(['speed', 'lane'])

        if random_type == 'speed':
            speed = self.road[i, j, 2] - 1  
            if speed < 0:
                speed == 0

            x, new_speed, new_pos = self.p_sight(speed, i, j)

            if new_speed <= 0:  
                self.road[i,j,2] = 0
                return self.road

            else:
                self.road[i,j,2] = new_speed
                self.road[int(x), int(new_pos - 1), :] = self.road[i, j, :]
                self.road[i,j,:] = 0
                return self.road
            

        elif random_type == 'lane':
            speed = self.road[i, j, 2] 
            x, new_speed, new_pos = self.p_lane_switch(speed, i, j)
            #  if x == 1 and i == 0: #Check to see if we dare overtake
            #      if np.random.uniform(0,1,1)[0] > self.road[i,j,3]:
            #          x, new_speed, new_pos = self.p_front(speed, i, j)
            if new_speed <= 0:
                self.road[i,j,2] = 0
                return self.road

            else:
                self.road[i,j,2] = new_speed
                self.road[int(x), int(new_pos - 1), :] = self.road[i, j, :]
                self.road[i,j,:] = 0
                return self.road

    def p_sight(self, speed, x, y):
        not_legal = True
        while not_legal:
            jump = int(y+speed+1)

            if jump > self.road.shape[1]:
                road_tmp_front = [self.road[x,y+1:,0], self.road[x,:jump%self.road.shape[1],0]]
                road_tmp_front = np.hstack(road_tmp_front)
                
                road_tmp_side = [self.road[(x+1)%2,y:,0], self.road[(x+1)%2,:jump%self.road.shape[1],0]]
                road_tmp_side = np.hstack(road_tmp_side)

                front = np.any(road_tmp_front != 0)
                side  = np.any(road_tmp_side != 0)
                jump = jump%self.road.shape[1]
            else:
                front = np.any(self.road[x, y+1:jump, 0] != 0)
                side  = np.any(self.road[(x+1)%2, y:jump, 0] != 0)

            if front == False or side == False:
                not_legal = False
                if side == True: #we continue in our present lane
                    return x, speed, jump
                elif side == False and front == False:
                    if x == 1:
                        return (x+1)%2 , speed, jump
                    else:
                        return x, speed, jump
                else:
                    #  print('I chose to switch it up')
                    #  print((x+1)%2 , speed, jump)
                    return (x+1)%2 , speed, jump
            else:
                speed -= 1
            if speed <= 0:
                not_legal == False
                return x, 0, y+1
        
    def _p_sight(self, speed_in, x, y):

        for speed in np.arange(1,speed_in+1,)[::-1]:
            new_pos = int(y+speed)

            if new_pos > self.road.shape[1]:
                road_tmp_front = [self.road[x,y+1:,0], self.road[x,:new_pos%self.road.shape[1],0]]
                road_tmp_front = np.hstack(road_tmp_front)
            
                road_tmp_side = [self.road[(x+1)%2,y:,0], self.road[(x+1)%2,:new_pos%self.road.shape[1],0]]
                road_tmp_side = np.hstack(road_tmp_side)

                front = np.any(road_tmp_front != 0)
                side  = np.any(road_tmp_side != 0)
                new_pos = new_pos%self.road.shape[1]#print('1', road_tmp_side)
                #print('2',np.array(road_tmp_side).flatten())
                #road_tmp_side = np.hstack(road_tmp_side)
                #print('3',road_tmp_side)
                #exit()
            else:
                front = np.any(self.road[x, y+1:new_pos, 0] != 0)
                side  = np.any(self.road[(x+1)%2, y:new_pos, 0] != 0)
                #  print('infront')
                #  print(self.road[x, y+1:jump,0])
                #  print(self.road[(x+1)%2, y:jump,0])

            if front == False and side == True:
                return x, speed, new_pos
                
            elif front == True and side == False:
            #         #  print('I chose to switch it up')
            #         #  print((x+1)%2 , speed, jump)
                return (x+1)%2 , speed, new_pos

            elif front == False and side == False: 
                if x == 1:
                    return (x+1)%2 , speed, new_pos
                else:
                    return x, speed, new_pos
        if speed <= 0:
            return x, 0, y+1   

    def p_lane_switch(self, speed, x, y):
        legal = False
        while not legal:
            jump = int(y+speed+1)
            if jump > self.road.shape[1]:
                road_tmp_front = [self.road[x,y+1:,0], self.road[x,:jump%self.road.shape[1],0]]
                road_tmp_front = np.hstack(road_tmp_front)
                
                road_tmp_side = [self.road[(x+1)%2,y:,0], self.road[(x+1)%2,:jump%self.road.shape[1],0]]
                road_tmp_side = np.hstack(road_tmp_side)

                front = np.any(road_tmp_front != 0)
                side  = np.any(road_tmp_side != 0)
                jump = jump%self.road.shape[1]

            else:
                front = np.any(self.road[x, y+1:jump, 0] != 0)
                side  = np.any(self.road[(x+1)%2, y:jump, 0] != 0)

            if front == False and side == False: 
                return (x+1)%2 , speed, jump

            if front == False or side == False:
                legal = True
                if side == True: #we continue in our present lane
                    return x, speed, jump
                elif side == False and front == False:
                    if x == 1:
                        return (x+1)%2 , speed, jump
                    else:
                        return x, speed, jump
                else:
                    return (x+1)%2 , speed, jump
            else:
                speed -= 1
            if speed <= 0:
                legal = True
                return x, 0, y+1

    def p_front(self, speed, x, y):
        legal = False
        while not legal:
            new_pos = int(y+speed+1)

            if new_pos > self.road.shape[1]:
                road_tmp = [self.road[x,y+1:,0], self.road[x,:new_pos%self.road.shape[1],0]]
                road_tmp = np.hstack(road_tmp)
                front = np.any(road_tmp != 0)
                new_pos = new_pos%self.road.shape[1]
            else:
                front = np.any(self.road[x, y+1:new_pos, 0] != 0)

            if front == False:
                legal = True
                return x, speed, new_pos
            else:
                speed -= 1

            if speed <= 0:
                legal = True
                return x, 0, y+1

                

    
